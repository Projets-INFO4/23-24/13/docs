# Documentation for Turtle RTK App (offline mode)

## Introduction

This new version of the NTRIP application is intented to operate in an "offline" mode. In this version, unlike the first one, enables users to receive GNSS RTK (Real-Time Kinematic) data via Bluetooth Low Energy (BLE) without the need for an internet connection.

This mode is essential for OpenStreetMap contributors and other users who require high-accuracy positioning when they are outside the coverage of cellular networks or in remote areas.

## Operating Modes

The application has two main operating modes:

1. **Online Mode (mode 1)**: In this mode, the application connects to an NTRIP server via the internet to receive GNSS RTK corrections. This mode is useful when the user has access to the internet and the NTRIP server is reachable.

2. **LoRa Offline Mode (mode2)**: The focus of this documentation. In this mode, the application receives GNSS RTK data via BLE from a compatible Android device. This mode is useful when the user is in an area without internet coverage or when the user wants to use a local base station.

## Requirements for Offline Mode

In order to use the application in offline mode, the following requirements have to be met:

- A smartphone with BLE capability and the NTRIP application installed.
- BLE-enabled GNSS RTK module (rover) that supports the required data outputs.

## Usage Instructions

The user should follow these instructions in order to interact with the Rover via the TurtleRTK mobile application:

1 - **Switch to Offline Mode**: Users can select the offline mode from the application's main screen.
Settings -> Mode Selection -> Mode 2 (Offline Mode)

2- **Connect to the Rover**: The application scans for BLE devices and allows the user to connect to the rover.

3- **Data Reception**: Once connected, the application begins to receive and display GNSS RTK data from the rover.

4- **Location Mocking**: If required, users can enable location mocking to use the received RTK data as the phone's location. This feature may need special permissions and settings adjustments on the smartphone.

## LoRa Offline Mode Functionality

In the offline mode, the application disables the caster (or base) features. From this point on, it solely depends on the BLE connection to receive the following data from the rover:

- Latitude
- Longitude
- Altitude
- Vertical Accuracy
- Horizontal Accuracy

Therefore, all Caster features will be inaccessible to the user in this mode.

The rover, equipped with a GNSS RTK module, calculates these parameters and transmits them to the smartphone application via BLE.

## Application Limitations

The application only supports Android devices with potential future support for iOS.

## Useful links

- [Previous team Documentation](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/02/docs/-/blob/1f3cedebfd5ac89d6ee4b950fac9de61484d59e7/report.md)

- [BLE Manager Documentation](https://innoveit.github.io/react-native-ble-manager/)

- [FakeTraveler (Native Java Mock Location App)](https://github.com/mcastillof/FakeTraveler/tree/master/app/src/main)


## What we did

We will discuss the things we worked on since we got the project.

### Functional Core

1. **`AppStore` class**: contains all the global variables used throught the app:

- added `serviceUUID` variable that specifies the rover service UUID
- added `characteristicUUIDs` that contains all characteristic UUIDs (5 in total) of values that we which to get from Rover.
- added `locationValues` that contains the values received, when new values are sent, those variables will be updates **(Note: this is where the mock location gets it's values)**

2. **`BluetoothManager` class**: controls Communication througth `BLE`.

- changed `onDataUpdate` function, this function is a listener function that it's called on each characteristic notify, in other word, this function reads values received by `BLE`, and update values in `locationValues` accordingly.

3. **LocationPackage**: We added a `LocationPackage`, it's a native package that consist of two main Java classes : `NativeLocationManager.java` and `NetworkLocationManager.java`. Although the first class is enough to change Mock Location, the latter can help obtain more accurate Location values.
4. **LocationManaging**: This class consists of an encapsulation of Java native Package (previously mentioned), which contains the `pushLocation()` method. This function calls the native java `pushLocation(double lat, double lon)` method of both Network and GPS classes, providing them with the values stored in `locationValues` variable (set and updated by BLE listener) in `AppStore` class. These references are done in intervals of *100 ms*. This refresh rate is needed so the GPS signal doesn't override Mock Location.

### User Interface

1. **Mode Selection Page**: Since the app has 2 modes, we added a mode selection `ModeSelectionScreen.tsx` page in the Settings category. In `mode 1`, the app conserve's last's year project's functionalities (via NTrip), whereas in `mode 2`, the app only communicates with the Rover through `BLE`. In the latter mode, `Caster Screen` can't be accessed,  neither Caster Settings Screen.
2. **Mock Location Enable/Disable Button**: In the `Rover Screen`, 2 buttons were added: one to enable Mock Location, and the other to disable Mock Location (*Note that default postitions are set to 0, so if Mock Location is enabled while not receiving, position 0 will be mocked*).
3. **Details Button**: added a *«Decent»* Details Button for `Bluetooth Devices Item` UI in `PeripheralListItem.tsx`, as it was only 3 dots before, and they were very hard to click.
4. **Responsive Scan Button**: Scan Button in `HeaderRoverScreen.tsx`, changes color to Green when scanning, and then turns back to white when done scaning (*Current Scanning duration is 5 seconds*).
5. **Real-time Display of received data**: On recording screen, we display received values such as *latitude*, *longitude*, *altitude*, *horizontal accuracy*, *vertical accuracy*.

## What you need to know

In the following part, we will explain important pending tasks you need to know if you wish to contribute to this project.

### Functional Core

1. **Background Location Refresh**: App currently stops Mocking Location when app runs in the background, to solve this you may need to dive into [Native code](https://reactnative.dev/docs/native-modules-android), or you can use [Headless JS](https://reactnative.dev/docs/headless-js-android.html) to run tasks in background.
2. **Background Listener**: Currently we are not sure if the BLE listener could run in the background, so if you succeed to run background Mock Location update, you need to verify that values are also updated by the listeners in the background.
3. **Accuracy Values**: Accuracy values need to be corrected, numbers are transmitted correctly but value is not correct *(we are getting values of order 10⁻⁴⁰)*, to do that we may look into file `BluetoothManager.ts`, in `onDataUpdate` function.

### User Interface

To enhance user interaction and provide a more intuitive experience, some additional elements would have been appreciated. Here are the following improvements are proposed:

1. **Advanced Data Display:** Introduce a dedicated page or section within the app to display the GNSS RTK data in a more detailed and visually appealing format, such as graphs and charts. This allows users to easily understand the accuracy and quality of the positioning data. It is recommended to use React Native libraries like [`react-native-chart-kit`](https://www.npmjs.com/package/react-native-chart-kit) or [`victory-native`](https://www.npmjs.com/package/victory-native) to create interactive charts and graphs for data visualization.
2. **User-friendly Settings:** Redesign the settings page to group related options and include explanatory tooltips or help icons next to each setting, making it easier for users to configure the app according to their needs. It's recommended to use UI Frameworks such as [`react-native-paper`](https://reactnativepaper.com/) and [`react-native-vector-icons`](https://www.npmjs.com/package/react-native-vector-icons)for intuitive icons for settings.
3. **Interactive Tutorial:** Include an interactive tutorial or walkthrough for first-time users, highlighting key features and guiding them through the process of connecting to a rover and using the app in offline mode. This can greatly enhance user onboarding and app usability. Here are the recommended settings: [`react-native-app-intro-slider`](https://www.npmjs.com/package/react-native-app-intro-slider) for a simple and effective introductory slider or [`react-native-onboarding-swiper`](https://www.npmjs.com/package/react-native-onboarding-sliders).
