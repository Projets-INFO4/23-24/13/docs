# Setup instructions

The target board is an [ESP-WROOM-32](https://www.espressif.com/en/producttype/esp32-wroom-32), although most ESP32 boards should work as well (not tested). We're using 2 of them, each paired with an RTK module (over I2C) and a LoRa module (over SPI).

## Install Arduino IDE
The [modern 2.x version](https://www.arduino.cc/en/software) is recommended, but if your distro doesn't package it and you have problems with the AppImage version, the [legacy 1.8.x version](https://www.arduino.cc/en/software#legacy-ide-18x) (scroll down) is also compatible.

There is an [Arduino extension for VSCode](https://marketplace.visualstudio.com/items?itemName=vsciot-vscode.vscode-arduino) as well, although it is still in Preview and can be a little less plug-and-play. The [PlatformIO IDE](https://platformio.org/platformio-ide) is also a popular choice but not tested.  
Finally, both versions of the official IDE support quick file refresh, which means you can code on your favorite editor and only use the IDE to compile/upload.

## Install the ESP32 Board Definitions
Board definitions explain to the Arduino IDE how to build and upload for a specific board *([SparkFun has a nice tutorial](https://learn.sparkfun.com/tutorials/installing-board-definitions-in-the-arduino-ide/all) on this)*. We need to add Espressif's repository so that the board manager can download definitions for all their ESP32 boards.

[`https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json`](https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json)

Paste this URL in the IDE's settings (**File** -> **Preferences** -> **Additional board manager URLs**)  
Then press **Ctrl+Shift+B** to open the Boards manager or go to **Tools** menu -> **Board** -> **Boards manager**. Search for "esp" and install the ***esp32 by Espressif Systems*** package.  
![Screenshot of the esp32 package](/./images/esp32_ide_package.png)

> ***Note:** We are using the latest version of the package, `v2.0.14` If you encounter issues compiling/uploading, try switching to the older `v2.0.7` used by the previous project*

You should now be able to select your board in the **Tools** -> **Board** menu. Use `ESP32 Dev Module`.

## Add the libraries

The [`SparkFun_u-blox_GNSS_v3`](https://github.com/sparkfun/SparkFun_u-blox_GNSS_v3/) and [`SX128XLT`](https://github.com/StuartsProjects/SX12XX-LoRa) libraries, respectively for the RTK module and LoRa module, are included directly in the Git repository and must be added to the Arduino include path.

If you don't know your Arduino sketchbook path, it is indicated in the IDE preferences (*File* -> *Preferences*) as **Sketchbook location**, usually `~/Arduino` on Linux.
Open this directory and find the `libraries` dir: this is where you have to copy the libraries. 

> *For each library, make sure to copy its full directory, the one that contains the `library.properties` file. This is the metadata file that tells the IDE which version it is.*

## Check the settings
For the `RTK_Base` and `RTK_Rover` projects, check that the the config variables in the `Settings.h` file are correct.

## Verify/Compile
You can now use the :ballot_box_with_check: **Verify** button in the IDE to compile the code.  
If you get an error, go to the Preferences and check ***Show verbose output during :white_check_mark: compilation :white_check_mark: upload*** to get more details about errors. Here, you can also tweak the ***Compiler warnings*** level, or enable ***Display line numbers*** to find the error line more easily.

## Select your board
If it's not already done, select your board in the **Tools** -> **Board** menu.  
Under **Tools**, select the **Port** as well, usually `/dev/ttyUSB`something.
> *You can see the selected board and port at any moment in the bottom-right corner of the IDE*.

## Upload
Finally, upload your code to the board with the :ballot_box_with_check:**Upload** button. If there's any code changes, it will automatically compile the code as well before uploading.

## Bonus: Serial Monitor
The serial monitor is what allows you to see messages from the board, and send characters as well.

Open it with `Ctrl+Shift+M` or with the :mag_right: button in the top-right.  
If you see weird characters, you may need to change the **Baud rate** (default 9600) by looking for the line `Serial.begin(<some number>)` in your code.
