# Documentation for Base Station

[BackToInitialReadMe](../../README.md)

## Project General Documentation

This project involves a device that receives [RTCM](https://www.rtcm.org/) data from an [RTK](https://fr.wikipedia.org/wiki/RTK) (Real-Time Kinematics) module using I2C, processes it, and sends it via LoRa. The device also provides position and accuracy data over Serial.

### Libraries Used

- `SparkFun_u-blox_GNSS_v3`, `sfe_bus`, `u-blox_Class_and_ID`, `u-blox_GNSS`, `u-blox_config_keys`, `u-blox_external_typedefs`, `u-blox_structs`: SparkFun U-blox GNSS libraries.
- `SPI`, `SX128XLT`: LoRa library and SPI because the LoRa device is SPI based.
- `Settings`: Custom settings file for constants, frequencies, LoRa settings, etc.
- `ArduinoQueue`: Library for creating a queue to store RTCM sentences.

### Global Variables

- `myGNSS`: An instance of the `SFE_UBLOX_GNSS` class.
- `LT`: An instance of the `SX128XLT` class.
- `TXPacketL`: Variable for storing length of transmitted packet.
- `storageRTCM`: A struct for storing info about RTCM frame.
- `rtcmQueue`: A queue for storing RTCM sentences.
- `rtcmCounter`, `loraPacketCounter`: Counters for tracking the type of incoming byte inside RTCM frame and the order of Lora packet.
- `enqueuing_bytes`, `dequeuing_bytes`: Counters for tracking the number of bytes enqueued and dequeued from the RTCM queue.

### Setup

The `setup()` function initializes the serial communication, WiFi for OTA updates, SPI and LoRa, and I2C for communicating with the RTK module. It also sets up the RTK module to output RTCM3 messages.

### Loop

The `loop()` function handles OTA updates if enabled, receives RTCM data from the RTK module, stores it in a queue, and sends it via LoRa. It also prints the position and accuracy data over Serial.

### Functions

- `checkBaseValues()`: Retrieves and prints the position and accuracy data from the RTK module.
- `sendLoraPacket()`: Sends a LoRa packet.
- `processRTCM()`: Processes incoming RTCM data and stores it in a queue.
- `packet_is_OK()`: Processes a valid LoRa packet.
- `packet_is_Error()`: Handles errors in received LoRa packets.
- `crc24q()`: Calculates the CRC-24Q checksum for RTCM data.

### Note

This documentation assumes familiarity with LoRa, RTK, and I2C. It also assumes knowledge of the hardware setup and the custom `Settings` file.

## Function Documentation

### checkBaseValues()

This function retrieves and prints the position and accuracy data from the RTK module.

```c
void checkBaseValues()
{
  // ... retrieve and print the position and accuracy data ...
}
```

- myGNSS.getHPPOSLLH(): Retrieves the position and accuracy data from the RTK module. The function only responds when a new position is available.
- The function then retrieves and prints the latitude, longitude, altitude, horizontal accuracy, and vertical accuracy.

### sendLoraPacket()

This function sends a LoRa packet.

```c
void sendLoraPacket(uint8_t * ptr_buffer, uint16_t size, uint16_t order, uint16_t type)
{
  // ... send the LoRa packet ...
}
```

- The function first checks if the size of the packet is less than or equal to the maximum LoRa size. If so, it appends 2 additional bytes to the end of the LoRa packet for detecting the order of the Lora packet, and sends the packet using LT.transmit().
- If the size of the packet is greater than the maximum LoRa size, the function divides the packet into smaller packets and sends each one separately.

### processRTCM()

This function processes incoming RTCM data and stores it in a queue.

```c
void DevUBLOXGNSS::processRTCM(uint8_t incoming)
{
  // ... process the RTCM data and store it in a queue ...
}
```

- The function first checks if a new RTCM sentence has been detected. If so, it initializes the RTCM counter and sets the current sentence type to RTCM.
- The function then processes the RTCM data based on the RTCM counter. It stores the data in a struct, calculates the checksum, and checks if all bytes have been received.
- If all bytes have been received and the checksum matches, the function stores the RTCM data in a queue.

### packet_is_OK()

This function processes a valid LoRa packet.

```c
void packet_is_OK()
{
  // ... process the valid LoRa packet ...
}
```

- The function first extracts the RTCM data from the received LoRa packet. This involves checking the SYN (Synchronization) bit, the number of remaining packets, and the order of the current packet.
- If ready_for_sendingRTCM is true, it means a full RTCM sentence has been received. The function then calculates the number of missed packets (if any), prints some results to the console, and sends the RTCM data to the RTK module using myGNSS.pushRawData().
- After sending the RTCM data, the function calls checkBaseValues() to retrieve and print the position and accuracy data from the RTK module.
- Finally, the function resets some variables in preparation for the next LoRa packet.

### packet_is_Error()

This function handles errors in received LoRa packets.

```c
void packet_is_Error()
{
  // ... handle the LoRa packet error ...
}
```

- The function first reads the IRQ (Interrupt Request) status register of the LoRa device using LT.readIrqStatus().
- The function then checks if there was an RX (Receive) timeout. If so, it prints "RXTimeout" to the console. Otherwise, it increments the errors counter, prints some error information to the console, and prints the names of the IRQ registers that are set.

### crc24q()

This function calculates the CRC-24Q checksum for RTCM data.

```c
void crc24q(uint8_t incoming, uint32_t *checksum)
{
  // ... calculate the CRC-24Q checksum ...
}
```

- The function takes two arguments: the incoming byte and the current checksum.
- The function then calculates the new checksum based on the incoming byte and the current checksum using the CRC-24Q polynomial.

For more information about the LoRa library, please refer to the SX128XLT library documentation.
For more information about the RTK library, please refer to the SparkFun U-blox GNSS library documentation.
