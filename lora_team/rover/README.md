# Documentation for Rover

[BackToInitialReadMe](../../README.md)

## Project General Documentation

This project involves a device that receives RTCM (Real-Time Kinematics) data via LoRa, processes it, and sends it to an RTK module using I2C. The device in turn provides position and accuracy data over BLE (Bluetooth Low Energy).

### Libraries Used

- `BLEDevice`, `BLEUtils`, `BLEServer`: ESP32 BLE libraries.
- `SparkFun_u-blox_GNSS_v3`, `sfe_bus`, `u-blox_Class_and_ID`, `u-blox_GNSS`, `u-blox_config_keys`, `u-blox_external_typedefs`, `u-blox_structs`: SparkFun U-blox GNSS libraries.
- `SPI`, `SX128XLT`: LoRa library and SPI because the LoRa device is SPI based.
- `Settings`: Custom settings file for constants, frequencies, LoRa settings, etc.

### Global Variables

- `myGNSS`: An instance of the `SFE_UBLOX_GNSS` class.
- `LT`: An instance of the `SX128XLT` class.
- `RXpacketCount`, `errors`: Counters for received packets and errors.
- `RXBUFFER`, `RTCM_FULL_BUFFER`: Buffers for received packets and full size RTCM sentence.
- `RXPacketL`, `PacketRSSI`, `PacketSNR`: Variables for storing length, RSSI, and SNR of received packet.
- `ready_for_sendingRTCM`, `totalSize_of_RTCM`, `nb_of_packets`: Variables for processing LoRa packet before sending out the RTCM sentences to RTK of rover.
- `order_of_packet`, `lora_received_counter`, `nb_of_packets_missed`, `time_stamp`: Variables for tracking the order and status of received packets.
- `lat_char_ble`, `lon_char_ble`, `alt_char_ble`, `ach_char_ble`, `acv_char_ble`: BLE characteristics for latitude, longitude, altitude, accuracy, and vertical accuracy.

### Setup

The `setup()` function initializes the serial communication, WiFi for OTA updates, SPI and LoRa, BLE, and I2C for communicating with the RTK module.

### Loop

The `loop()` function reads incoming LoRa packets, processes them (reassembling any fragmented payload), pushes them to the RTK module, and fetches location data to send via BLE. It optionally handles OTA updates if enabled in the config. 

### Functions

- `receiveLoraPacket()`: Receives a LoRa packet and checks if it's valid.
- `packet_is_OK()`: Processes a valid LoRa packet, extracts the RTCM data, and sends it to the RTK module.
- `packet_is_Error()`: Handles errors in received LoRa packets.
- `sendBLE_LLH()`: Sends the position and accuracy data over BLE.
- `getPositionAndAccuracy()`: Retrieves the position and accuracy data from the RTK module for benchmarking purposes.

### BLE API

1. The BLE characteristics are created in the `setup()` function.
2. The following characteristics are updated in the `sendBLE_LLH()` function.
3. The notifications are sent just after the data is updated in the characteristics in the `sendBLE_LLH()` function.
4. All of the characteristics are read-only and Little Endian unless otherwise specified.
5. The UUID for each characteristic is defined in the custom `Settings` file.

`Latitude`: The latitude of the device, in degrees * 10^-7
- Type : int32_t
- UUID : `beb5483e-36e1-4688-b7f5-ea07361b26a8`

`Longitude`: The longitude of the device, in degrees * 10^-7
- Type : int32_t
- UUID : `73d0beb1-a959-4710-94b9-912c4128188a`

`Altitude`: The altitude of the device, in millimeters
- Type : int32_t
- UUID : `22b90c31-14c6-4a5e-9e36-a49f45c55646`

`Horizontal Accuracy`: The horizontal accuracy of the device, in meters
- Type : `float32`
- UUID : `2de7d184-5b1c-43c4-8fa4-0d1e42f791b2`

`Vertical Accuracy`: The vertical accuracy of the device, in meters
- Type : `float32`
- UUID : `c4ca4c6b-fd92-46df-858a-1d01e218f50c`

### Note

This documentation assumes familiarity with LoRa, RTK, BLE, and I2C. It also assumes knowledge of the hardware setup and the custom `Settings` file.

## Function Documentation

### receiveLoraPacket()

This function receives a LoRa packet and checks if it's valid.

```c++
void receiveLoraPacket()
{
  RXPacketL = LT.receive(RXBUFFER, RXBUFFER_SIZE, 20000, WAIT_RX); //wait for a packet to arrive with 20 seconds (20000mS) timeout
  PacketRSSI = LT.readPacketRSSI();              //read the recived RSSI value
  PacketSNR = LT.readPacketSNR();                //read the received SNR value

  if (RXPacketL == 0)                            //if the LT.receive() function detects an error, RXpacketL is 0
  {
    packet_is_Error();
  }
  else
  {
    packet_is_OK();
  }
}
```

- LT.receive(): Receives a LoRa packet. It takes four arguments: the buffer to store the received data, the buffer size, the timeout in milliseconds, and the mode (WAIT_RX in this case, which means the function waits for a packet to arrive).
- LT.readPacketRSSI(): Reads the RSSI (Received Signal Strength Indicator) value of the received packet.
- LT.readPacketSNR(): Reads the SNR (Signal-to-Noise Ratio) value of the received packet.
- If RXPacketL is 0, it means there was an error in receiving the packet, and packet_is_Error() is called. Otherwise, packet_is_OK() is called.

### packet_is_OK()

This function processes a valid LoRa packet, extracts the RTCM data, and sends it to the RTK module.

```c++
void packet_is_OK()
{
  // ... extract RTCM data from the packet ...

  // done receiving a full Lora packet
  if(ready_for_sendingRTCM)
  {
    // ... calculate the number of missed packets ...

    //print out some results to console, comment if needed
    Serial.printf("Timestamp=%d, RTCM size=%d, order=%d, packets missed=%d\n", time_stamp, totalSize_of_RTCM, order_of_packet, nb_of_packets_missed);

    //sending RTCM sentence to ZED F9P
    myGNSS.pushRawData(RTCM_FULL_BUFFER, totalSize_of_RTCM, false);  //push the raw RTCM data to the RTK module

    //get position and accuracy
    getPositionAndAccuracy();

    //send the data through BLE
    sendBLE_LLH();

    // reset some variables for making it ready to process the next turn
    ready_for_sendingRTCM = false;
    nb_of_packets = 0;
    totalSize_of_RTCM = 0;
  }
}
```

- The function first extracts the RTCM data from the received LoRa packet. This involves checking the SYN (Synchronization) bit, the number of remaining packets, and the order of the current packet.
- If ready_for_sendingRTCM is true, it means a full RTCM sentence has been received. The function then calculates the number of missed packets (if any), prints some results to the console, and sends the RTCM data to the RTK module using myGNSS.pushRawData().
- myGNSS.pushRawData(): Sends raw RTCM data to the RTK module. It takes three arguments: the buffer containing the data, the size of the data, and a flag indicating whether the data is UBX (U-blox proprietary) or not.
- After sending the RTCM data, the function calls getPositionAndAccuracy() to retrieve the position and accuracy data from the RTK module, and sendBLE_LLH() to send this data over BLE.
- Finally, the function resets some variables in preparation for the next LoRa packet.

### packet_is_Error()

This function handles errors in received LoRa packets.

```c++
void packet_is_Error()
{
  uint16_t IRQStatus;
  IRQStatus = LT.readIrqStatus();                   //read the LoRa device IRQ status register

  if (IRQStatus & IRQ_RX_TIMEOUT)                   //check for an RX timeout
  {
    Serial.print(F(" RXTimeout"));
  }
  else
  {
    errors++;
    Serial.print(F(" PacketError"));
    Serial.print(F(",RSSI,"));
    Serial.print(PacketRSSI);
    Serial.print(F("dBm,SNR,"));
    Serial.print(PacketSNR);
    Serial.print(F("dB,Length,"));
    Serial.print(LT.readRXPacketL());               //get the device packet length
    Serial.print(F(",Packets,"));
    Serial.print(RXpacketCount);
    Serial.print(F(",Errors,"));
    Serial.print(errors);
    Serial.print(F(",IRQreg,"));
    Serial.print(IRQStatus, HEX);
    LT.printIrqStatus();                            //print the names of the IRQ registers set
    Serial.println();
  }

  delay(250);                                       //gives a longer buzzer and LED flash for error
}
```

- LT.readIrqStatus(): Reads the IRQ (Interrupt Request) status register of the LoRa device.
- The function checks if there was an RX (Receive) timeout. If so, it prints "RXTimeout" to the console. Otherwise, it increments the errors counter, prints some error information to the console, and prints the names of the IRQ registers that are set.

### sendBLE_LLH()

This function sends the position and accuracy data over BLE.

```c++
void sendBLE_LLH()
{
  //Query module. The module only responds when a new position is available
  if (myGNSS.getHPPOSLLH())
  {
    // ... retrieve and print the position and accuracy data ...

    // Send the data over BLE
    lat_char_ble->setValue((uint8_t*)&latitude, sizeof(latitude));
    lon_char_ble->setValue((uint8_t*)&longitude, sizeof(longitude));
    alt_char_ble->setValue((uint8_t*)&msl, sizeof(msl));
    ach_char_ble->setValue((uint8_t*)&hor_accuracy, sizeof(hor_accuracy));
    acv_char_ble->setValue((uint8_t*)&vert_accuracy, sizeof(vert_accuracy));
  } else {
    Serial.println("No new position available");
  }
}
```

- myGNSS.getHPPOSLLH(): Retrieves the position and accuracy data from the RTK module. The function only responds when a new position is available.
- The function then sends this data over BLE using the setValue() method of the BLE characteristics. This method takes two arguments: a pointer to the data, and the size of the data.

### getPositionAndAccuracy()

This function retrieves the position and accuracy data from the RTK module.

```c++
void getPositionAndAccuracy()
{
  //Query module. The module only responds when a new position is available
  if (myGNSS.getHPPOSLLH())
  {
    // ... retrieve and print the position and accuracy data ...
  }
}
```

- myGNSS.getHPPOSLLH(): Retrieves the position and accuracy data from the RTK module. The function only responds when a new position is available.
- The function then retrieves and prints the latitude, longitude, altitude, horizontal accuracy, and vertical accuracy.

For more information about the LoRa library, please refer to the [SX128XLT library documentation](https://github.com/Lora-net/SX126x-Arduino). 
For more information about the RTK library, please refer to the [SparkFun U-blox GNSS library documentation](https://github.com/sparkfun/SparkFun_u-blox_GNSS_Arduino_Library). 
For more information about BLE, please refer to the [ESP32 BLE library documentation](https://github.com/espressif/arduino-esp32/tree/master/libraries/BLE).
