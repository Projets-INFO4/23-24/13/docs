# LoRa / embedded Team

[BackToInitialReadMe](../README.md)

## Here are the links to the different documentations of the parts of the project:

- [Setup instructions](./SETUP.md)
- [Base documentation](./base/README.md)
- [Rover documentation](./rover/README.md)

## Useful Links

### GNSS RTK

- [GNSS Centimeter Caster (RTK)](https://air.imag.fr/index.php/Caster_GNSS_centimétrique_(RTK)): Dedicated page to the centimeter-level GNSS real-time positioning system (RTK).
- [RTKLIB](https://www.rtklib.com/): Open-source software for GNSS data processing.
- [u-blox ZED-F9P Module](https://www.u-blox.com/en/product/zed-f9p-module): Page for the u-blox ZED-F9P GNSS module.
- [ZED-F9P Datasheet](https://content.u-blox.com/sites/default/files/ZED-F9P-04B_DataSheet_UBX-21044850.pdf): Technical specification for the u-blox ZED-F9P GNSS module.
- [Interface Description of u-blox F9 Module](https://content.u-blox.com/sites/default/files/documents/u-blox-F9-HPG-1.32_InterfaceDescription_UBX-22008968.pdf): Detailed description of the u-blox F9 GNSS module interface. Includes a description of the UBX message format.

### NTRIP & LoRa

- [NTRIP LoRa Caster](https://air.imag.fr/index.php/Caster_NTRIP_LoRa): Dedicated page to the NTRIP broadcast system using LoRa technology.
- [Development of an iOS/Android RTK NTRIP Application](https://air.imag.fr/index.php/Dévelopement_d’une_application_iOS/Android_RTK_NTrip): Page presenting the development of a mobile application for receiving RTK corrections via the NTRIP protocol.
- [LoRa Gitlab Project](https://gricad-gitlab.univ-grenoble-alpes.fr/groups/lora-rtk): Gitlab project dedicated to the development of LoRa technology for RTK applications.

### Documentation & Resources

- [Gitlab Project from Last Year (Turtle NTRIP App)](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/02): Gitlab project of the Turtle NTRIP application from the previous academic year.
- [CampusIoT Documentation](https://github.com/campusiot/tutorial): Documentation of the CampusIoT project.
- [Binex File Format](https://www.unavco.org/data/gps-gnss/data-formats/binex/binex.html#software): Page presenting the Binex file format used for GNSS data.
- [RTKLIB Source File (rtcm3.c)](https://github.com/tomojitakasu/RTKLIB/blob/master/src/rtcm3.c#L1830): Source file of the RTKLIB software containing the RTCM3 message management functions.
- [New Additions in RTCM3 and MSMV](https://www.tersus-gnss.com/tech_blog/new-additions-in-rtcm3-and-What-is-msmv): Blog post presenting the new features of the RTCM3 protocol and MSMV technology.
- [RTCM Version 3 Documentation](https://docs.novatel.com/Waypoint/Content/Utilities/RTCM_Version_3.htm): Documentation of the RTCM Version 3 protocol.
- [RTCM Message Cheat Sheet](https://www.use-snip.com/kb/knowledge-base/an-rtcm-message-cheat-sheet/): Re

### RTK Message Summary

- [Centipede RTK Documentation](https://docs.centipede.fr/docs/centipede/2_RTK.html): Documentation of the Centipede RTK software.
- [Interface Description (Protocols)](https://content.u-blox.com/sites/default/files/documents/u-blox-F9-HPG-1.32_InterfaceDescription_UBX-22008968.pdf): Detailed description of the protocols supported by the u-blox F9 module.
