## Session 1 (29/01)

We couldn't meet with the IESE project team beforehand to discuss the ESP32 hardware
and their projects (timetable issues), so Nicolas Palix showed us the hardware platform.

After that, we discussed the project's structure (since we're in a bit of a special
configuration with one group on two projects), notably the hardware and software
components involved.

- App team: Set-up React Native dev environment (Android Studio, AVD, dev server...)
- LoRa team: Set-up Arduino IDE and tried building the `RTK_Base` project from Gitlab
(board definitions, libraries...)

## Session 2 (12/02)

Still no meeting with the IESE but they joined our discord server.

We all (App and LoRa team) discussed about the different diagrams representing the "old" and the "new" state to 
really grasp where we will be interacting.

![picture of the old and new diagrams](images/image.jpeg)

After that, we tried building/uploading on the real boards to see how everything was connecting together after understanding everything thanks to the diagrams

Here you can see the rover installation
![picture of the rover installation](images/image-1.jpeg)

![picture of the rover installation](images/image-2.jpeg)

We apparently found a LoRa signal while booting up the LoRa Base

![picture of the rover installation](images/image-3.jpeg)

## Session 3 (04/03)
Yet another recap on the global architecture, this time discussing more about the carrier links (LoRa, BLE...) and the message formats

**App team**
- Explored the codebase to find how to pick a BLE device and receive corrections through it
- (Ali) Restructured the double Git repo to have our own copy of each submodule and avoid modifying previous years' repos

**LoRa team**
- Explored the RTK chip's datasheet (U-blox ZED-F9P) and library documentation to understand the exact message formats supported (UBX / NMEA / RTCM)
- Unplanned in-person meeting with IESE team who happened to be here, discussed many interesting points
  - message formats, apparently the current code already ditched NMEA and uses RTCM (binary) from end to end? Will have to ask N. Palix
  - their exact building/flashing setup (matches our config)
  - the constellation measurements they were conducting
  - explained Git so that they could publish their results and docs on Gitlab

## Session 4 (11/03)

This session was mainly focused on the documentation of the project : Gantt diagrams, SWOT matrices ...

Finalized the drawing of the general architecture in order to prepare for the mid-project defence due at the end of this week (15/03)

**App team** : implemented the front page of the Turtle RTK application that separates the 2 modes (mode 1 = 2022/2023 project, mode 2 = our version of the project)

**LoRa team** : attempt to understand the structure of RTCM3 messages sent over LoRa, from Sparkfun u-blox datasheets. Successfully decoded a message manually.

## Session 5 (12/03)

This session was mainly focused on the making of the presentation (cf Session 6)

**App team** : started research on the location problem, we need to find a way to override the phone's location with the corrected data. This process is harder than expected because of the non-native software, React Native doesn't expose every Android API so tweaks are required.

**LoRa team** : reflection about choices that are open to us. 
1. Use the UBX protocol, that may require some time to understand and get used to.
2. Create our own and custom message format, that will require some testing to find what pieces of information are needed in order to reduce the data transmitted over LoRa.


## Session 6 (15/03) : mid-project defense 

Quick fixes on the presentation slides.

[Link to the Canva presentation](./Présentation mi parcours.pdf)

Important remarks from the teachers : 
1. Explain the complex names like UBX, LoRa, NMEA, RTCM, ...
2. Explain the context of the project, the "global" meaning of the project.

## Session 8 (19/03)
App team
- Prototyping on React "Native Modules" and on Android mock location system
- Working on disabling Caster features to the user when accessing via Mode 2.  
- Mode 1 and Mode 2 selection in app is fully functionnal

LoRa team
- Troubleshooting (lots of it) because the base seemingly stopped sending messages. Experimented with changing the hard-coded reference position
- Some statistics on the average and max sizes of RTCM messages actually sent, using logs from previous experiments
- Experimented with [RTKlib](https://github.com/tomojitakasu/RTKLIB), compiling it for the ESP32 *(failed, way too memory-hungry)* then running it locally to try and decode messages

## Session 9 (22/03)
App team
- Switch between Mode 1 (NTRIP) and Mode 2 (LoRa offline) is now implemented and functionnal
- Native Modules for Android are working as required, app can now be set as a *mock location* app.
- Added buttons on Rover screen to toggle Mock Location
- Setting a Mock location works (location is changing for a short duration), needs further tests and improvements
- App is now able to disable Caster settings and Caster features whilst in Mode 2. However, Caster features remain accessible via Mode 1.  

LoRa team
- Continuing the experiments with RTKlib, still trying to decode MSM4 messages
- Automated the decoding program to speed up inspecting messages from logs 
- Tried decoding using the available apps shipped with the lib
- More general research on RTKlib and RTCM

## Session 10 (25/03)

App Team
- Fixed mock Location persistency problem. However, when app runs in the background, mock location stops working. 
In order for the mock location to be enabled in the system, the app has to be running on a split screen. 
- Planned out the new rover screen, where we will be able to see the connected device, from a UI pov as well as a FC pov.  

LoRa team
- Still trying to decode MSM messages, a bit of progress with `gdb` to explore the structs interactively. Found some values for raw observation fields *(SNR, LLI, code, carrier-phase, pseudorange, Doppler frequency)* but still very obscure without a good understanding of the math behind GNSS. 
- Prototyping the Bluetooth link between rover and smartphone
  - First attempt with the `BluetoothSerial.h` library and its examples + a [serial terminal app](https://play.google.com/store/apps/details?id=de.kai_morich.serial_bluetooth_terminal) on the phone, worked out-of-the-box
  - Then switched to Bluetooth Low Energy (BLE), as it's the more modern way and we seek energy efficiency. A bit more complex (new concepts, Services and Characteristics) but we successfully transmitted data to a paired phone and read it with [Nordic](https://www.nordicsemi.com/)'s awesome [nRF Connect](https://play.google.com/store/apps/details?id=no.nordicsemi.android.mcp) tool

- Problems while reading the latitude/longitude/altitude from the rover's RTK module (null values). Discussing with the IESE team, they had issues with it as well in the past, which means we dont know if it ever worked...

![Photo of the test phone with BLE readings from nRF Connect](images/phone_with_ble_characteristics.jpg "Photo of the test phone with BLE readings from nRF Connect")


## Session 11 (26/03)

App Team 
- Implemented  the UI for the new Rover screen, by creating a mock device that is always connected to Bluetooth. However, the issue is that the mock device is not added to peripherals' list of connected devices.  
![Screenshot of mock device in the emulator](images/mock_device_capture.png){width=60%}

LoRa Team 
- More work on the BLE.

## Session 12 (29/03)

App Team 
- Worked with LoRa team in order to bridge between the Rover and the App using BLE. The service and characteristics are detected, but the [`BleManagerDidUpdateValueForCharacteristic`](https://innoveit.github.io/react-native-ble-manager/events/#blemanagerdidupdatevalueforcharacteristic) event listener doesn't seem to work, even though we're enabling it with `startNotification()`

LoRa Team
- More tweaks on the BLE part
- Added documentation on the Base and Rover
- Added general documentation

## Session 13 (2/04)

App Team: 
- Small tweaks in UI implementation, including changing the selecting mode screen and making the "details" button larger for the Rover devices in Rover category of the application. 
- Worked on updating the coordinates from the rover. We have some issues with interpreting these values)
- Trying to make mock location work in the background (even when app is not running) -> Unsuccessful. 

## Session 14 (3/04)

Team meeting with tutor in order to discuss our current project advancement and also discuss the eventual problems that we are facing. 
