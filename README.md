# :satellite_orbital: LoRa-RTK | INFO4 2023/2024 (DOCS)

*This is the docs repository of the LoRa-RTK 2023/24 project.*

:gear: **[Code repository](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/23-24/13/code)**

:scroll: **[Logbook](Caster_GNSS_centim%C3%A9trique_(RTK)_ET_D%C3%A9velopement_d%E2%80%99une_application_iOS_Android_RTK_NTrip_info4_2023_2024.md)**

:memo: **[Final report](rapport.pdf)**

:frame_photo: **[Final presentation](presentation.pdf)**

:book: **[TurtleRTK app documentation folder](app_team)**

:book: **[LoRa/embedded documentation folder](lora_team)**

## Description

This project aims to enable high-accuracy [GNSS-RTK](https://en.wikipedia.org/wiki/Real-time_kinematic_positioning) positioning on a smartphone or mobile device (Android/iOS) without an Internet connection.
It is a long-running student project, initially developed around the [NTRIP](https://en.wikipedia.org/wiki/Networked_Transport_of_RTCM_via_Internet_Protocol) protocol to enhance accuracy for [OpenStreetMap](https://www.openstreetmap.org/) contributors.

The Caster and the Rover consist of two ESP32 boards, each paired with a [SparkFun U-Blox ZED-F9P](https://www.u-blox.com/en/product/zed-f9p-module) GNSS-RTK module, and a [Semtech SX128x series](https://www.semtech.com/products/wireless-rf/lora-connect) LoRa module. The code running on these is a fork of the original [LoRa-RTK project on Gricad-Gitlab](https://gricad-gitlab.univ-grenoble-alpes.fr/lora-rtk), implemented a few years earlier.  
 The rover also includes the [Turtle NTRIP](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/02/docs) mobile app, built with React Native, that receives the fine position data to make it available to other apps. In the "online" NTRIP-based mode, the app acts as the NTRIP receiver that transmits correction data to the RTK module.

![Architecture diagram](images/lora_rtk_architecture.png)

NTRIP / Centipede mode :

- Caster receives GPS data, sends RTCM corrections over Centipede
- Mobile app receives corrections from Centipede, sends them to the ESP32 rover
- Rover with RTK module computes fine location from GNSS + corrections and sends it back to the app

LoRa / offline mode :

- Caster receives GPS data, sends RTCM corrections over LoRa (direct wireless)
- Rover receives corrections, computes fine location with its RTK module
- Finally the rover sends fine location to the app

## PBS diagram

The PBS (Product Breakdown Structure) diagram provides a detailed visualization of the components and sub-components involved in the project.
<details><summary>Click to expand diagram</summary>
![PBS diagram](images/PBS.png)
</details>

## A few definitions

**BLE**: Bluetooth Low Energy. A power-conserving variant of Bluetooth, designed for use in applications where low power consumption is critical.

**ESP32**: A series of low-cost, low-power System-on-a-Chip (SoC) microcontrollers with integrated Wi-Fi and dual-mode Bluetooth.

**GNSS-RTK**: Global Navigation Satellite System - Real-Time Kinematic. A satellite navigation technique used to enhance the precision of position data derived from satellite-based positioning systems, using a fixed reference station.

**NMEA** (National Marine Electronics Association): A specification for communication between marine electronics such as navigational devices and GPS receivers. It also applies to the formatting of GNSS data.

**RTCM** (Radio Technical Commission for Maritime services): A communication protocol for sending differential GPS (DGPS) to a GPS receiver from a secondary source like a radio receiver.

**NTRIP**: Networked Transport of RTCM via Internet Protocol. A protocol used for streaming differential GPS data over the internet.

**UBX**: A binary protocol developed by U-Blox to work with GNSS data. It allows for efficient communication with GNSS receivers.

**OpenStreetMap**: An open-source, collaborative project to create a free editable map of the world.

**RTKlib**: An open-source library designed to process GNSS-RTK positioning data. One of the few remaining open implementations of the closed standard.

**Semtech SX128x**: A series of [LoRa](https://en.wikipedia.org/wiki/LoRa) transceiver chips designed for long-range, low-power communication in IoT networks.

**SparkFun U-Blox ZED-F9P**: A high-accuracy GNSS module from U-Blox for precise navigation, capable of RTK.

**I2C**: Inter-integrated Circuit, is a communications protocol common in microcontroller-based systems, particularly for interfacing with sensors and memory devices.

## Sequence Diagram

![Here is a sequence diagram that explains the different interactions between the components, and how the user interacts with this system via the NTrip application:](images/seq_diag.png)

## Gantt Diagram

For our Gantt diagram, we decided to use the open-source JavaScript library [Frappe Gantt](https://github.com/frappe/gantt). it's a simple, interactive, modern Gantt chart library for the web with drag and resize, dependencies and time scales features.

In order to visualize the Gantt diagram, you need to clone this repository and open the HTML file in your browser, located at the following path:

```html
gantt/node_modules/frappe-gantt/dist/gantt.html
```

if you don't have Git available, you can [download the folder directly with GitLab](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/23-24/13/docs/-/archive/main/docs-main.zip?path=gantt/node_modules/frappe-gantt/dist).

You can also see a preview screenshot of the diagram below:
<details><summary>Click to expand preview</summary>
![Preview screenshot of the Gantt diagram](images/gantt_preview.png)
</details>

## Base and Rover general documentation

- [Lora team documentation](lora_team/README.md)
- [App team documentation](app_team/README.md)

## Acknowledgments

Contributors:

- Pamella Hani
- Nicolas Picaud
- Ali Fawaz
- Eliott Sammier

Tutor: Nicolas Palix
